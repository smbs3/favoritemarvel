import { Category } from '../../enum/category.enum';
import { IsEnum, IsInt, IsPositive } from 'class-validator';

export class CreateFavoriteDto {

  @IsInt()
  @IsPositive()
  marvelId: number;

  @IsEnum(Category)
  category: Category;
}
