import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Favorite } from '../database/models/favorite.entity';
import { Repository } from 'typeorm';
import { CreateFavoriteDto } from './dto/create-favorite.dto';
import { UpdateFavoriteDto } from './dto/update-favorite.dto';

@Injectable()
export class FavoritesService {
  private readonly logger = new Logger('FavoriteService');
  constructor(
    @InjectRepository(Favorite)
    private readonly favoriteRepository: Repository<Favorite>,
  ) {}

  private handleExceptions(error: any) {
    if (error.errno === 1062) throw new BadRequestException(error.sqlMessage);
    this.logger.error(error);
  }

  async createFavorite(createFavoriteDto: CreateFavoriteDto, userId: string) {
    try {
      const newFav = this.favoriteRepository.create({...createFavoriteDto, userId });
      await this.favoriteRepository.save(newFav);
      return newFav;
    } catch (error) {
      this.handleExceptions(error);
    }
  }

  async getFavorites(userId: string, category = '') {
    let params = {};
    if (category) params = { category };
    try {
      const favorites = await this.favoriteRepository.find({
        where: { ...params, userId },
      });
      return favorites;
    } catch (error) {
      return false;
    }
  }

  async getOneFavorite(id: string) {
    try {
      const favorite = await this.favoriteRepository.findOneOrFail({
        where: { id },
      });
      return favorite;
    } catch (error) {
      throw new NotFoundException('Favorite not found');
    }
  }

  async updatefavorite(id: string, updateFavoriteDto: UpdateFavoriteDto) {
    const favorite = await this.favoriteRepository.preload({
      id,
      ...updateFavoriteDto,
    });
    if (!favorite) throw new NotFoundException('Favorite not found');

    try {
      await this.favoriteRepository.save(favorite);
      return favorite;
    } catch (error) {
      this.logger.error(error);
    }
  }

  async deletefavorite(id: string) {
    try {
      const favorite = await this.favoriteRepository.findOneOrFail({
        where: { id },
      });
      await this.favoriteRepository.remove(favorite);
    } catch (error) {
      throw new NotFoundException('Favorite not found');
    }
  }
}
