import {
  Controller,
  Query,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseUUIDPipe,
  UseGuards,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { FavoritesService } from './favorites.service';
import { CreateFavoriteDto } from './dto/create-favorite.dto';
import { UpdateFavoriteDto } from './dto/update-favorite.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { User } from '../decorators/user.decorator';

@UseGuards(JwtAuthGuard)
@Controller('favorites')
export class FavoritesController {
  constructor(private readonly favoritesService: FavoritesService) {}

  @Post()
  createFavorite(
    @User() userId: string,
    @Body() createFavoriteDto: CreateFavoriteDto,
  ) {
    const data = this.favoritesService.createFavorite(
      createFavoriteDto,
      userId,
    );
    if (data) return data;
    throw new HttpException('Failed to add favorite', HttpStatus.NOT_FOUND);
  }

  @Get(':id')
  getOneFavorite(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.favoritesService.getOneFavorite(id);
  }

  @Get()
  getFavorites(@User() userId: string, @Query() query: any) {
    return this.favoritesService.getFavorites(userId, query.category);
  }

  @Patch(':id')
  updateFavorite(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() UpdateFavoriteDto: UpdateFavoriteDto,
  ) {
    return this.favoritesService.updatefavorite(id, UpdateFavoriteDto);
  }

  @Delete(':id')
  deleteFavorite(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.favoritesService.deletefavorite(id);
  }
}
