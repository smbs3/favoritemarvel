import { User } from './user.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../utils/BaseEntity';
import { Category } from '../../enum/category.enum'

@Entity()
export class Favorite extends BaseEntity {

  @Column()
  marvelId: number;

  @Column({ type: 'enum', enum: Category, default: Category.CHARACTER })
  category: Category;

  @Column({ name: 'user_id' })
  userId: string;

  @ManyToOne(() => User, (entity) => entity.favorites)
  user: User;
}
