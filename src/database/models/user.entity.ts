import { Favorite } from './favorite.entity';
import { Entity, Column, OneToMany } from 'typeorm';
import { BaseEntity } from '../../utils/BaseEntity';

@Entity()
export class User extends BaseEntity{

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @OneToMany(() => Favorite, (entity) => entity.user)
  favorites: Favorite[];
}
