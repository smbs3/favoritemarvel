import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { User } from '../models/user.entity';
import { hashPassword } from '../../utils';

export default class CreateUser implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(User)
      .values([
        {
          id: '1b7dc58e-9c63-4bc0-b493-e0366ba28603',
          username: 'isbs',
          password: hashPassword('170391'),
        },
        {
          id: 'e05aae73-42e2-4354-bf9a-13b419570a61',
          username: 'amorcito',
          password: hashPassword('12345'),
        },
        {
          id: 'e28aae73-75e2-4354-bf9a-13b427570a61',
          username: 'hades',
          password: hashPassword('23456'),
        },
      ])
      .execute();
  }
}
