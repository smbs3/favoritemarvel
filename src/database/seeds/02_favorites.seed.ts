import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { Favorite } from '../models/favorite.entity';
import { Category } from '../../enum/category.enum';

export default class CreateFavorites implements Seeder {
  public async run(factory: Factory, connection: Connection) {
    await connection
      .createQueryBuilder()
      .insert()
      .into(Favorite)
      .values([
        {
          id: '7c5739fd-312f-4f61-be8a-fc60cc97dd36',
          category: Category.COMIC,
          marvelId: 14,
          userId: '1b7dc58e-9c63-4bc0-b493-e0366ba28603',
        },
        {
          id: '6b5869fc-20fa-4278-82b3-87a5601d6c28',
          category: Category.COMIC,
          marvelId: 14,
          userId: 'e05aae73-42e2-4354-bf9a-13b419570a61',
        },
        {
          id: '6c0653d2-879d-41f9-a54f-045dc5a716ef',
          category: Category.CHARACTER,
          marvelId: 1017100,
          userId: 'e05aae73-42e2-4354-bf9a-13b419570a61',
        },
        {
          id: '39bec48c-5a65-43de-8a67-46768fa7c6bc',
          category: Category.CHARACTER,
          marvelId: 1011334,
          userId: 'e05aae73-42e2-4354-bf9a-13b419570a61',
        },
      ])
      .execute();
  }
}
