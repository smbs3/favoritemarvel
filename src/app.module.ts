import { Module } from '@nestjs/common';
import { CharacterModule } from './character/character.module';
import { ComicModule } from './comic/comic.module';
import { FavoritesModule } from './favorites/favorites.module';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmAsyncConfig } from './config/typeorm.config';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    UserModule,
    FavoritesModule,
    CharacterModule,
    ComicModule,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
