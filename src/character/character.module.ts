import { Module } from '@nestjs/common';
import { CharacterService } from './character.service';
import { CharacterController } from './character.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [CharacterController],
  providers: [CharacterService],
  imports: [
    HttpModule.register({
      baseURL: `${process.env.PUBLIC_API_MARVEL_URL}/characters`,
      params: {
        ts: 1,
        apikey: process.env.PUBLIC_API_MARVEL_KEY,
        hash: process.env.PUBLIC_API_MARVEL_HASH,
      },
    }),
  ],
})
export class CharacterModule {}
