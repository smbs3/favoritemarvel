import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { UserService } from 'src/user/user.service';
import { User } from '../database/models/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async verifyUser(loginDto: LoginDto): Promise<any> {
    const user = await this.userService.getUserForUsername(loginDto.username);
    if (!user) return false;
    const match = await bcrypt.compare(loginDto.password, user.password);
    if (match) {
      return user;
    }
    throw new HttpException(
      `username or password incorrect ${match}`,
      HttpStatus.BAD_REQUEST,
    );
  }

  async login(user: User) {
    const payload = {
      sub: user.id,
    };
    return { access_token: this.jwtService.sign(payload) };
  }
}
