import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpException,
  HttpStatus,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@UseInterceptors(ClassSerializerInterceptor)
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  createUser(@Body() newUser: CreateUserDto) {
    return this.userService.createUser(newUser);
  }

  @Get(':id')
  getOneUser(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.userService.getOneUser(id);
  }

  @Get()
  getUsers() {
    const data = this.userService.getUsers();
    if (data) return data;
    throw new HttpException('Failed to get all users', HttpStatus.NOT_FOUND);
  }

  @Patch(':id')
  updateUser(
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() UpdateUserDto: UpdateUserDto,
  ) {
    return this.userService.updateUser(id, UpdateUserDto);
  }

  @Delete(':id')
  deleteUser(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.userService.deleteUser(id);
  }
}
