import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from '../database/models/user.entity';
import { hashPassword } from '../utils';

@Injectable()
export class UserService {
  private readonly logger = new Logger('UserService');
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}
  private handleExceptions(error: any) {
    if (error.errno === 1062) throw new BadRequestException(error.sqlMessage);
    this.logger.error(error);
  }

  async createUser(createUserDto: CreateUserDto) {
    try {
      createUserDto.password = hashPassword(createUserDto.password);
      const newUser = await this.userRepository.save({ ...createUserDto });
      return newUser;
    } catch (error) {
      this.handleExceptions(error);
    }
  }

  getUsers() {
    try {
      return this.userRepository.find({ relations: ['favorites'] });
    } catch (error) {
      return false;
    }
  }

  async getOneUser(id: string) {
    try {
      const user = await this.userRepository.findOneOrFail({
        where: { id },
        relations: ['favorites'],
      });
      return user;
    } catch (error) {
      return new NotFoundException('User not found');
    }
  }

  async getUserForUsername(username: string) {
    try {
      const user = await this.userRepository.findOneOrFail({
        where: { username },
      });
      return user;
    } catch (error) {
      throw new NotFoundException('Username not found');
    }
  }

  async updateUser(id: string, updateUserDto: UpdateUserDto) {
    const user = await this.userRepository.preload({ id, ...updateUserDto });

    if (!user) throw new NotFoundException('User not found');
    try {
      await this.userRepository.update(id, updateUserDto);
      return user;
    } catch (error) {
      this.handleExceptions(error);
    }
  }

  async deleteUser(id: string) {
    try {
      const user = await this.userRepository.findOneOrFail({ where: { id } });
      await this.userRepository.remove(user);
    } catch (error) {
      throw new NotFoundException('User not found');
    }
  }
}
