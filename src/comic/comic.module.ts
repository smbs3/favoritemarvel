import { Module } from '@nestjs/common';
import { ComicService } from './comic.service';
import { ComicController } from './comic.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  controllers: [ComicController],
  providers: [ComicService],
  imports: [
    HttpModule.register({
      baseURL: `${process.env.PUBLIC_API_MARVEL_URL}/comics`,
      params: {
        ts: 1,
        apikey: process.env.PUBLIC_API_MARVEL_KEY,
        hash: process.env.PUBLIC_API_MARVEL_HASH,
      },
    }),
  ],
})
export class ComicModule {}
